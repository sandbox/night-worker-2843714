<?php

/**
 * @file
 * Contains \Drupal\remove_inactive_user\Form\ModuleConfigurationForm.
 */

namespace Drupal\remove_inactive_user\Form;

use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a form that configures forms module settings.
 */
class ModuleConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'remove_inactive_user_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'remove_inactive_user.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL) {
    $period = array(0 => 'disabled')     // array_map für Umwandlung in Text  array array_map ( callable $callback , array $array1 [, array $... ] )
        +  array(604800 => '1 week')
        +  array(1209600 => '2 weeks')
        +  array(1814400 => '3 weeks')
        +  array(2419200 => '4  weeks')
        +  array(2592000 => '1 month')
        +  array(7776000 => '3 months')
        +  array(15552000 => '6 months')
        +  array(23328000 => '9 months')
        +  array(31536000 => '1 year')
        +  array(47088000 => '1 year 6 months')
        +  array(63072000 => '2 years')
        +  array(94608000 => '3 years')
     ;  
    $warn_period = array(0 => 'disabled') 
      + array(86400 => '1 day')
      + array(172800 => '2 days')
      + array(259200 => '3 days')
      + array(604800 => '1 week')
      + array(1209600 => '2 weeks')
      + array(1814400 => '3 weeks')
      + array(2592000 => '1 month')
      ;
    $mail_variables = ' %username, %useremail, %lastaccess, %period, %sitename, %siteurl';
    
    $config = $this->config('remove_inactive_user.settings');
    
     // Set administrator e-mail
  $form['remove_inactive_user_admin_email_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Administrator e-mail'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['remove_inactive_user_admin_email_fieldset']['admin_email'] = array(
    '#type' => 'textfield',
    '#title' => t('E-mail addresses'),
    '#default_value' => $config->get('admin_email'),
    '#description' => t('Supply a comma-separated list of e-mail addresses that will receive administrator alerts. Spaces between addresses are allowed.'),
    '#maxlength' => 256,
    '#required' => TRUE,
  );

  // Inactive user notification.
  $form['remove_inactive_user_notification'] = array(
    '#type' => 'fieldset',
    '#title' => t('Inactive user notification'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['remove_inactive_user_notification']['notify_admin'] = array(
    '#type' => 'select',
    '#title' => t("Notify administrator when a user hasn't logged in for more than"),
    '#default_value' => $config->get('notify_admin'),
    '#options' => $period,
    '#description' => t("Generate an email to notify the site administrator that a user account hasn't been used for longer than the specified amount of time.  Requires crontab."),
  );
  $form['remove_inactive_user_notification']['notify'] = array(
    '#type' => 'select',
    '#title' => t("Notify users when they haven't logged in for more than"),
    '#default_value' => $config->get('notify'),
    '#options' => $period,
    '#description' => t("Generate an email to notify users when they haven't used their account for longer than the specified amount of time.  Requires crontab."),
  );
  $form['remove_inactive_user_notification']['notify_text'] = array(
    '#type' => 'textarea',
    '#title' => t('Body of user notification e-mail'),
    '#default_value' => $config->get('notify_text'),
    '#cols' => 70,
    '#rows' => 10,
    '#description' => t('Customize the body of the notification e-mail sent to the user.') . ' ' . t('Available variables are:') . $mail_variables,
    '#required' => TRUE,
  );

  // Automatically block remove inactive users.
  $form['block_remove_inactive_user'] = array(
    '#type' => 'fieldset',
    '#title' => t('Automatically block remove inactive users'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['block_remove_inactive_user']['auto_block_warn'] = array(
    '#type' => 'select',
    '#title' => t('Warn users before they are blocked'),
    '#default_value' => $config->get('auto_block_warn'),
    '#options' => $warn_period,
    '#description' => t('Generate an email to notify a user that his/her account is about to be blocked.'),
  );
  $form['block_remove_inactive_user']['block_warn_text'] = array(
    '#type' => 'textarea',
    '#title' => t('Body of user warning e-mail'),
    '#default_value' => $config->get('block_warn_text'),
    '#cols' => 70,
    '#rows' => 10,
    '#description' => t('Customize the body of the notification e-mail sent to the user when their account is about to be blocked.') . ' ' . t('Available variables are:') . $mail_variables,
    '#required' => TRUE,
  );
  $form['block_remove_inactive_user']['auto_block'] = array(
    '#type' => 'select',
    '#prefix' => '<div><hr></div>', // For visual clarity
    '#title' => t("Block users who haven't logged in for more than"),
    '#default_value' => $config->get('auto_block'),
    '#options' => $period,
    '#description' => t("Automatically block user accounts that haven't been used in the specified amount of time.  Requires crontab."),
  );
  $form['block_remove_inactive_user']['notify_block'] = array(
    '#type' => 'checkbox',
    '#title' => t('Notify user'),
    '#default_value' => $config->get('notify_block'),
    '#description' => t('Generate an email to notify a user that his/her account has been automatically blocked.'),
  );
  $form['block_remove_inactive_user']['block_notify_text'] = array(
    '#type' => 'textarea',
    '#title' => t('Body of blocked user account e-mail'),
    '#default_value' => $config->get('block_notify_text'),
    '#cols' => 70,
    '#rows' => 10,
    '#description' => t('Customize the body of the notification e-mail sent to the user when their account has been blocked.') . ' ' . t('Available variables are:') . $mail_variables,
    '#required' => TRUE,
  );
  $form['block_remove_inactive_user']['notify_block_admin'] = array(
    '#type' => 'checkbox',
    '#title' => t('Notify administrator'),
    '#default_value' => $config->get('notify_block_admin'),
    '#description' => t('Generate an email to notify the site administrator when a user is automatically blocked.'),
  );

  // Automatically delete remove inactive users.
  $form['delete_remove_inactive_user'] = array(
    '#type' => 'fieldset',
    '#title' => t('Automatically delete remove inactive users'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['delete_remove_inactive_user']['auto_delete_warn'] = array(
    '#type' => 'select',
    '#title' => t('Warn users before they are deleted'),
    '#default_value' => $config->get('auto_delete_warn'),
    '#options' => $warn_period,
    '#description' => t('Generate an email to notify a user that his/her account is about to be deleted.'),
  );
  $form['delete_remove_inactive_user']['delete_warn_text'] = array(
    '#type' => 'textarea',
    '#title' => t('Body of user warning e-mail'),
    '#default_value' => $config->get('delete_warn_text'),
    '#cols' => 70,
    '#rows' => 10,
    '#description' => t('Customize the body of the notification e-mail sent to the user when their account is about to be deleted.') . ' ' . t('Available variables are:') . $mail_variables,
    '#required' => TRUE,
  );
  $form['delete_remove_inactive_user']['auto_delete'] = array(
    '#type' => 'select',
    '#prefix' => '<div><hr></div>', // For visual clarity
    '#title' => t("Delete users who haven't logged in for more than"),
    '#default_value' => $config->get('auto_delete'),
    '#options' => $period,
    '#description' => t("Automatically delete user accounts that haven't been used in the specified amount of time.  Warning, user accounts are permanently deleted, with no ability to undo the action!  Requires crontab."),
  );
  $form['delete_remove_inactive_user']['preserve_content'] = array(
    '#type' => 'checkbox',
    '#title' => t('Preserve users that own site content'),
    '#default_value' => $config->get('preserve_content'),
    '#description' => t('Select this option to never delete users that own site content.  If you delete a user that owns content on the site, such as a user that created a node or left a comment, the content will no longer be available via the normal Drupal user interface.  That is, if a user creates a node or leaves a comment, then the user is deleted, the node and/or comment will no longer be accesible even though it will still be in the database.'),
  );
  $form['delete_remove_inactive_user']['notify_delete'] = array(
    '#type' => 'checkbox',
    '#title' => t('Notify user'),
    '#default_value' => $config->get('notify_delete'),
    '#description' => t('Generate an email to notify a user that his/her account has been automatically deleted.'),
  );
  $form['delete_remove_inactive_user']['delete_notify_text'] = array(
    '#type' => 'textarea',
    '#title' => t('Body of deleted user account e-mail'),
    '#default_value' => $config->get('delete_notify_text'),
    '#cols' => 70,
    '#rows' => 10,
    '#description' => t('Customize the body of the notification e-mail sent to the user when their account has been deleted.') . ' ' . t('Available variables are:') . $mail_variables,
    '#required' => TRUE,
  );
  $form['delete_remove_inactive_user']['notify_delete_admin'] = array(
    '#type' => 'checkbox',
    '#title' => t('Notify administrator'),
    '#default_value' => $config->get('notify_delete_admin'),
    '#description' => t('Generate an email to notify the site administrator when a user is automatically deleted.'),
  );


  // Automatically delete remove inactive users nologin.
  $form['delete_remove_inactive_user_nologin'] = array(
    '#type' => 'fieldset',
    '#title' => t('Automatically delete remove inactive users they never logged in'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['delete_remove_inactive_user_nologin']['auto_delete_nologin_warn'] = array(
    '#type' => 'select',
    '#title' => t('Warn users before they are deleted'),
    '#default_value' => $config->get('auto_delete_nologin_warn'),
    '#options' => $warn_period,
    '#description' => t('Generate an email to notify a user that his/her account is about to be deleted.'),
  );
  $form['delete_remove_inactive_user_nologin']['delete_warn_nologin_text'] = array(
    '#type' => 'textarea',
    '#title' => t('Body of user warning e-mail'),
    '#default_value' => $config->get('delete_warn_nologin_text'),
    '#cols' => 70,
    '#rows' => 10,
    '#description' => t('Customize the body of the notification e-mail sent to the user when their account is about to be deleted.') . ' ' . t('Available variables are:') . $mail_variables,
    '#required' => TRUE,
  );
  $form['delete_remove_inactive_user_nologin']['auto_delete_nologin'] = array(
    '#type' => 'select',
    '#prefix' => '<div><hr></div>', // For visual clarity
    '#title' => t("Delete users who never haven't logged in for more than"),
    '#default_value' => $config->get('auto_delete_nologin'),
    '#options' => $period,
    '#description' => t("Automatically delete user accounts that haven't been used in the specified amount of time.  Warning, user accounts are permanently deleted, with no ability to undo the action!  Requires crontab."),
  );
  $form['delete_remove_inactive_user_nologin']['notify_delete_nologin'] = array(
    '#type' => 'checkbox',
    '#title' => t('Notify user'),
    '#default_value' => $config->get('notify_delete_nologin'),
    '#description' => t('Generate an email to notify a user that his/her account has been automatically deleted.'),
  );
  $form['delete_remove_inactive_user_nologin']['delete_notify_nologin_text'] = array(
    '#type' => 'textarea',
    '#title' => t('Body of deleted user account e-mail'),
    '#default_value' => $config->get('delete_notify_nologin_text'),
    '#cols' => 70,
    '#rows' => 10,
    '#description' => t('Customize the body of the notification e-mail sent to the user when their account has been deleted.') . ' ' . t('Available variables are:') . $mail_variables,
    '#required' => TRUE,
  );


  $form['debug_remove_inactive_user'] = array(
    '#type' => 'fieldset',
    '#title' => t('Debug remove inactive users'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['debug_remove_inactive_user']['debug'] = array(
    '#type' => 'checkbox',
    '#title' => t('Debug'),
    '#default_value' => $config->get('debug'),
    '#description' => t('Additional debug informations in dlog.'),
  );

    
    return parent::buildForm($form, $form_state);
  }


  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->config('remove_inactive_user.settings')
      ->set('admin_email', $values['admin_email'])
      ->set('notify_admin', $values['notify_admin'])
      ->set('notify', $values['notify'])
      ->set('notify_text', $values['notify_text'])
      ->set('auto_block_warn', $values['auto_block_warn'])
      ->set('block_warn_text', $values['block_warn_text'])
      ->set('auto_block', $values['auto_block'])
      ->set('notify_block', $values['notify_block'])
      ->set('block_notify_text', $values['block_notify_text'])
      ->set('notify_block_admin', $values['notify_block_admin'])
      ->set('auto_delete_warn', $values['auto_delete_warn'])
      ->set('delete_warn_text', $values['delete_warn_text'])
      ->set('auto_delete', $values['auto_delete'])
      ->set('preserve_content', $values['preserve_content'])
      ->set('notify_delete', $values['notify_delete'])
      ->set('delete_notify_text', $values['delete_notify_text'])
      ->set('auto_delete_nologin_warn', $values['auto_delete_nologin_warn'])
      ->set('delete_warn_nologin_text', $values['delete_warn_nologin_text'])
      ->set('auto_delete_nologin', $values['auto_delete_nologin'])
      ->set('notify_delete_nologin', $values['notify_delete_nologin'])
      ->set('delete_notify_nologin_text', $values['delete_notify_nologin_text'])
      ->set('notify_delete_admin', $values['notify_delete_admin'])
      ->set('debug', $values['debug'])
      ->save();
  }
}
